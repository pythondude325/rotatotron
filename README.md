# Rotatotron
System tray icon that allows you to rotate the screen based on accelerometer
orientation.

## Dependencies
- `libappindicator-gtk3`