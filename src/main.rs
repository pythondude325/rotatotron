use anyhow::{anyhow, Context};
use std::process::Stdio;
use tokio::{
    sync,
    process,
};
use tokio_stream::{StreamExt};
mod orientation;
use orientation::Orientation;

use gtk::prelude::*;
use libappindicator::{AppIndicator, AppIndicatorStatus};

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    let mut monitor_process = process::Command::new("monitor-sensor")
        .arg("--accel")
        .stdout(Stdio::piped())
        .spawn()
        .context("Unable to spawn monitor-sensor")?;

    let monitor_stdout = monitor_process.stdout.take().unwrap();

    let mut orientation_stream = orientation::orientation_stream(monitor_stdout);

    let initial_orientation =
        orientation_stream.next().await
        .ok_or_else(|| anyhow!("No initial orientation found"))?
        .context("Failed to parse orientation")?;

    let (orientation_sensor_channel_tx, orientation_sensor_channel_rx) = sync::watch::channel(initial_orientation);
    let (orientation_change_channel_tx, orientation_change_channel_rx) = sync::watch::channel::<Option<Orientation>>(None);

    let _orientation_monitor = tokio::spawn(async move {
        let mut orientation_stream = orientation_stream;

        while let Some(result) = orientation_stream.next().await {
            let orientation = result?;
            orientation_sensor_channel_tx.send(orientation).expect("Unable to send orientation update");
        }

        anyhow::Result::<()>::Ok(())
    });
    
    let gtk_thread = tokio::task::spawn_blocking(move || {
        gtk::init().unwrap();

        let mut indicator = AppIndicator::new("Rotatotron", "");
        indicator.set_status(AppIndicatorStatus::Active);
        indicator.set_icon_theme_path("/usr/share/icons/breeze");
        indicator.set_icon_full("rotation-allowed", "icon");
        let mut m = gtk::Menu::new();

        let rotate_menu_item = gtk::MenuItem::with_label("Rotate");
        rotate_menu_item.connect_activate(move |_| {
            let new_orientation = *orientation_sensor_channel_rx.borrow();
            orientation_change_channel_tx.send(Some(new_orientation)).expect("unable to send orientation update");
        });
        m.append(&rotate_menu_item);

        let separator = gtk::SeparatorMenuItem::new();
        m.append(&separator);

        let quit_menu_item = gtk::MenuItem::with_label("Quit");
        quit_menu_item.connect_activate(|_| {
            gtk::main_quit();
        });
        m.append(&quit_menu_item);

        indicator.set_menu(&mut m);
        m.show_all();

        gtk::main();
    });

    let _orientation_switcher = tokio::spawn(async move {
        let mut orientation_change_channel_rx = orientation_change_channel_rx;

        loop {
            orientation_change_channel_rx.changed().await.expect("Orientation sender dropped");

            let new_orientation_message = *orientation_change_channel_rx.borrow();

            if let Some(new_orientation) = new_orientation_message {
                let switch_res = orientation::switch_orientation(new_orientation).await;

                if let Err(err) = switch_res {
                    eprintln!("encountered an error when switching orietation: {}", err);
                }
            }
        }
    });

    gtk_thread.await.context("GTK thread failed")?;

    monitor_process.kill().await?; // Kill the monitor subprocess

    Ok(())
}