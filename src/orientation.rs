use anyhow::anyhow;
use lazy_static::lazy_static;
use regex::Regex;
use tokio::{
    io::{self, AsyncBufReadExt},
    process,
};
use tokio_stream::{self as stream, StreamExt};

#[derive(Debug, Clone, Copy)]
pub enum Orientation {
    Normal,
    LeftUp,
    RightUp,
    BottomUp,
}

impl Orientation {
    fn to_xrandr_orientation(self) -> &'static str {
        match self {
            Orientation::Normal => "normal",
            Orientation::LeftUp => "left",
            Orientation::RightUp => "right",
            Orientation::BottomUp => "inverted",
        }
    }

    fn to_wacom_orientation(self) -> &'static str {
        match self {
            Orientation::Normal => "0",
            Orientation::LeftUp => "2",
            Orientation::RightUp => "1",
            Orientation::BottomUp => "3",
        }
    }

    fn to_generic_matrix_orientation(self) -> &'static str {
        match self {
            Orientation::Normal => "1 0 0 0 1 0 0 0 1",
            Orientation::LeftUp => "0 -1 1 1 0 0 0 0 1",
            Orientation::RightUp => "0 1 0 -1 0 1 0 0 1",
            Orientation::BottomUp => "-1 0 1 0 -1 1 0 0 1",
        }
    }
}

// This is mostly copied from kded_rotation's orientation-helper
// https://github.com/dos1/kded_rotation/blob/master/orientation-helper
pub async fn switch_orientation(orientation: Orientation) -> anyhow::Result<()> {
    const GENERIC_PROP: &str = "Coordinate Transformation Matrix";
    const WACOM_PROP: &str = "Wacom Rotation";

    lazy_static! {
        static ref XRANDR_PRIMARY_DISPLAY_REGEX: Regex =
            Regex::new(r"^(\S+) .*primary").expect("regex didn't compile");
        static ref XINPUT_DETECT_TOUCH_REGEX: Regex =
            Regex::new(r"(?i)Touchscreen|ELAN|Pen|Eraser|wacom|maXTouch|eGalaxTouch|IPTS")
                .expect("regex didn't compile");
        static ref GENERIC_PROP_REGEX: Regex =
            Regex::new(GENERIC_PROP).expect("regex didn't compile");
        static ref WACOM_PROP_REGEX: Regex = Regex::new(WACOM_PROP).expect("regex didn't compile");
    }

    let xrandr_process = process::Command::new("xrandr").output().await?;
    let xrandr_output = std::str::from_utf8(&xrandr_process.stdout)?;

    if let Some(primary_display) = xrandr_output
        .lines()
        .find_map(|l| XRANDR_PRIMARY_DISPLAY_REGEX.captures(l))
        .map(|c| c.get(1).expect("regex didn't match").as_str())
    {
        process::Command::new("xrandr")
            .arg("--output")
            .arg(primary_display)
            .arg("--rotate")
            .arg(orientation.to_xrandr_orientation())
            .spawn()?
            .wait()
            .await?;
    }

    let xinput_device_list_proc = process::Command::new("xinput")
        .args(["list", "--id-only"])
        .output()
        .await?;
    let xinput_device_list_output = std::str::from_utf8(&xinput_device_list_proc.stdout)?;

    for device_id in xinput_device_list_output.lines() {
        let xinput_props_proc = process::Command::new("xinput")
            .arg("list-props")
            .arg(device_id)
            .output()
            .await?;
        let props = std::str::from_utf8(&xinput_props_proc.stdout)?;

        if XINPUT_DETECT_TOUCH_REGEX.is_match(props) {
            if WACOM_PROP_REGEX.is_match(props) {
                process::Command::new("xinput")
                    .arg("set-prop")
                    .arg(device_id)
                    .arg(WACOM_PROP)
                    .arg(orientation.to_wacom_orientation())
                    .spawn()?
                    .wait()
                    .await?;
            } else if GENERIC_PROP_REGEX.is_match(props) {
                process::Command::new("xinput")
                    .arg("set-prop")
                    .arg(device_id)
                    .arg(GENERIC_PROP)
                    .arg(orientation.to_generic_matrix_orientation())
                    .spawn()?
                    .wait()
                    .await?;
            }
        }
    }

    Ok(())
}

impl std::str::FromStr for Orientation {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> anyhow::Result<Orientation> {
        Ok(match s {
            "normal" => Orientation::Normal,
            "left-up" => Orientation::LeftUp,
            "right-up" => Orientation::RightUp,
            "bottom-up" => Orientation::BottomUp,
            _ => return Err(anyhow!("invalid orientation string")),
        })
    }
}

pub fn orientation_stream<I: io::AsyncRead + Unpin>(
    input: I,
) -> impl stream::Stream<Item = anyhow::Result<Orientation>> {
    lazy_static! {
        static ref ORIENTATION_CHANGED_REGEX: Regex =
            Regex::new(r"(normal|left-up|right-up|bottom-up)").expect("regex didn't compile");
    }

    let buf_reader = io::BufReader::new(input);
    let lines = buf_reader.lines();

    stream::wrappers::LinesStream::new(lines).filter_map(
        |r| -> Option<anyhow::Result<Orientation>> {
            r.map_err(Into::<anyhow::Error>::into)
                .map(|l| {
                    if let Some(captures) = ORIENTATION_CHANGED_REGEX.captures(&l) {
                        let s = captures.get(1).expect("regex didn't match").as_str();
                        Some(s.parse::<Orientation>().expect("Bad orientation string"))
                    } else {
                        None
                    }
                })
                .transpose()
        },
    )
}
